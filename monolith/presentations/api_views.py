from django.http import JsonResponse
from common.json import ModelEncoder
from events.models import Conference
from .models import Presentation, Status
from django.views.decorators.http import require_http_methods
import json
import pika
from .producer import send_presentation_approved, send_presentation_rejected

class PresentationListEncoder(ModelEncoder):
    model = Presentation
    properties = ["title"]

    def get_extra_data(self, o):
        return { "status": o.status.name }

class ConferenceListEncoder(ModelEncoder):
    model = Conference
    properties = ["name"]

class PresentationDetailEncoder(ModelEncoder):
    model = Presentation
    properties = ['presenter_name', "presenter_email", "company_name", "title",
    "synopsis", "created","status","conference"]

    encoders = {
        "conference": ConferenceListEncoder(),
    }

    def get_extra_data(self, o):
        return { "status": o.status.name }

@require_http_methods(["GET", "POST"])
def api_list_presentations(request, conference_id):
    if request.method == "GET":
        presentations = Presentation.objects.filter(conference=conference_id)
        return JsonResponse({"presentations": presentations}, encoder=PresentationListEncoder,)
    else:
        content = json.loads(request.body)

    # Get the Presentation object and put it in the content dict
        conference = Conference.objects.get(id=conference_id)
        content["conference"] = conference

        presentation = Presentation.create(**content)
        return JsonResponse(
            presentation,
            encoder=PresentationDetailEncoder,
            safe=False,
        )
    """
    Lists the presentation titles and the link to the
    presentation for the specified conference id.

    Returns a dictionary with a single key "presentations"
    which is a list of presentation titles and URLS. Each
    entry in the list is a dictionary that contains the
    title of the presentation, the name of its status, and
    the link to the presentation's information.

    {
        "presentations": [
            {
                "title": presentation's title,
                "status": presentation's status name
                "href": URL to the presentation,
            },
            ...
        ]
    }
    """
    # presentations = [
    #     {
    #         "title": p.title,
    #         "status": p.status.name,
    #         "href": p.get_api_url(),
    #     }
    #     for p in Presentation.objects.filter(conference=conference_id)
    # ]
    # return JsonResponse({"presentations": presentations})






@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_presentation(request, pk):
    if request.method == "GET":
        presentation = Presentation.objects.get(id=pk)
        return JsonResponse(presentation, encoder = ShowPresentationEncoder, safe=False)
    elif request.method == "DELETE":
        count, _ = Presentation.objects.filter(id=pk)
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        try:
            presentation = Presentation.objects.filter(id=pk).update(**content)
            content["presentation"] = presentation
        except Presentation.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Presentation ID"}, status=400,
            )

    # copied from get detail
        presentation = Presentation.objects.get(id=pk)
        return JsonResponse(
            presentation,
            encoder=ShowPresentationEncoder,
            safe=False,
        )
    """
    Returns the details for the Presentation model specified
    by the pk parameter.

    This should return a dictionary with the presenter's name,
    their company name, the presenter's email, the title of
    the presentation, the synopsis of the presentation, when
    the presentation record was created, its status name, and
    a dictionary that has the conference name and its URL

    {
        "presenter_name": the name of the presenter,
        "company_name": the name of the presenter's company,
        "presenter_email": the email address of the presenter,
        "title": the title of the presentation,
        "synopsis": the synopsis for the presentation,
        "created": the date/time when the record was created,
        "status": the name of the status for the presentation,
        "conference": {
            "name": the name of the conference,
            "href": the URL to the conference,
        }
    }
    """
    # presentations = Presentation.objects.get(id=pk)
    # return JsonResponse(
    #     {
    #         "presenter_name": presentations.presenter_name,
    #         "company_name": presentations.company_name,
    #         "presenter_email": presentations.presenter_email,
    #         "title": presentations.title,
    #         "synopsis": presentations.synopsis,
    #         "created": presentations.created,
    #         "status": presentations.status.name,
    #         "conference": {
    #             "name": presentations.conference.name,
    #             "href": presentations.conference.get_api_url(),
    #         },
    #     }
    # )


@require_http_methods(["PUT"])
def api_approve_presentation(request, pk):
    presentation = Presentation.objects.get(id=pk)
    presentation.approve()
    send_presentation_approved(presentation)
    return JsonResponse(
        presentation,
        encoder=PresentationDetailEncoder,
        safe=False,
    )

@require_http_methods(["PUT"])
def api_reject_presentation(request, pk):
    presentation = Presentation.objects.get(id=pk)
    presentation.reject()
    send_presentation_rejected(presentation)
    return JsonResponse(
        presentation,
        encoder=PresentationDetailEncoder,
        safe=False,
    )
    # presentation = Presentation.objects.get(id=pk)
    # presentation.reject()
    # parameters = pika.ConnectionParameters(host="rabbitmq")
    # connection = pika.BlockingConnection(parameters)
    # channel = connection.channel()
    # channel.queue_declare(queue="presentations_rejections")
    # channel.basic_publish(
    #     exchange="",
    #     routing_key="presentations_rejections",
    #     body=json.dumps(
            # {
            #     "presenter_name":presentation.presenter_name,
            #     "presenter_email":presentation.presenter_email,
            #     "title":presentation.title
            # }
    #     ),
    # )
    # connection.close()

    # return JsonResponse(
    #     {
    #             "presenter_name":presentation.presenter_name,
    #             "presenter_email":presentation.presenter_email,
    #             "title":presentation.title
    #     }
    # )
