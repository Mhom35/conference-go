import pika
import json


def send_presentation_to_queue(presentation, queue_name):
    parameters = pika.ConnectionParameters(host="rabbitmq")
    connection = pika.BlockingConnection(parameters)
    channel = connection.channel()
    channel.queue_declare(queue=queue_name)
    presentation_dict = {
        "presenter_name":presentation.presenter_name,
        "presenter_email":presentation.presenter_email,
        "title":presentation.title
    }
    channel.basic_publish(
        exchange="",
        routing_key=queue_name,
        body=json.dumps(presentation_dict)
    )
    connection.close()

def send_presentation_rejected(presentation):
    send_presentation_to_queue(presentation, "presentations_rejections")


def send_presentation_approved(presentation):
    send_presentation_to_queue(presentation, "presentations_approvals")
